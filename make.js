const fs = require("fs")
const { execSync } = require("child_process")
const process = require('process')

const SANDBOX_DIR = "./sandbox"

const dirExists = dir => fs.existsSync(dir)

const readFile = fname => fs.readFileSync(fname)

const cdSandbox = () => process.chdir(SANDBOX_DIR)

const copySource = (src) => fs.copyFileSync(src, `${SANDBOX_DIR}/${src}`)

const createSandbox = () => {
    if (!dirExists(SANDBOX_DIR)) {
        fs.mkdirSync(SANDBOX_DIR)
    }
}

const curl = (url, flags) => {
    let args = ""
    for (let flag in flags) {
        args += `${flag} ${flags[flag]} `
    }
    execSync(`curl ${url} ${args}`)
}

const readConfig = () => JSON.parse(readFile("ligo.json"))
    
function fetchDepencencies(dependencies) {
    for (let dep in dependencies) {
        curl(dependencies[dep], {"--output": dep})
    }        
}

function compileCode(src, entrypoint, output) {
    try {
        execSync(`ligo compile-contract ${src} ${entrypoint} --output-file ${output}`)
    } catch(e) {
        console.log(e)
    }
}

function make() {
    const start = Date.now()
    const { dependencies, src, entrypoint, name } = readConfig()
    createSandbox()
    copySource(src)
    cdSandbox()
    fetchDepencencies(dependencies)
    compileCode(src, entrypoint, name)
    console.log(`Finished building contract... ${Date.now() - start}`)
}

make()
