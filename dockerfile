FROM node:latest AS node_base

FROM ubuntu:18.04

COPY --from=node_base . .

COPY . .

RUN chmod +x ./ligo
RUN cp ./ligo /usr/local/bin

# RUN ligo interpret --init-file testme.ligo "main (Increment (32), 10)"
# RUN ligo compile-contract testme.ligo main --output-file final.ligo

RUN node make.js